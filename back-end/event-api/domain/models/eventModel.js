const mongoose = require('mongoose');

let Schema = mongoose.Schema;

const events = new mongoose.Schema({
	name: String,
	description: String,
	key_words: {
		type:[String],
		required:true
	},
	location: {
	  type: {
		type: String, // Don't do `{ location: { type: String } }`
		enum: ['Point'], // 'location.type' must be 'Point'
		required: true
	  },
	  coordinates: {
		type: [Number],
		required: true
	  }
	}
  });

let eventsSchema = new Schema(events);

module.exports = mongoose.model('events', eventsSchema);
