const eventModel = require('../domain/models/eventModel');

/**
 * eventController.js
 *
 * @description :: Server-side logic for managing events.
 */
module.exports = {

  /**
   * eventController.list()
  **/
  near: (req, res) => {
    eventModel.find({
      location: {
        $geoWithin:{
          $centerSphere:[[ -23.5398178,-46.8791456 ], 1 ]}}
      }, (err, events) => {
      return err? res.status(500).json({
        message: 'Error when getting event.',
        error: err
      }) : res.json(events)
    })
  },

  list: (req, res) => {
    eventModel.find(req.query.where, req.query.fields, req.query.sort,   (err, events) => {
      if (err) {
        return res.status(500).json({
          message: 'Error when getting event.',
          error: err
        });
      }
      return res.json(events);
    });
  },

  /**
   * eventController.show()
   */
  show: (req, res) => {
    let id = req.params.id;
    eventModel.findOne({_id: id}, (err, event) => {
      if (err) {
        return res.status(500).json({
          message: 'Error when getting event.',
          error: err
        });
      }
      if (!event) {
        return res.status(404).json({
          message: 'No such event'
        });
      }
      return res.json(event);
    });
  },

  /**
   * eventController.create()
   */
  create: (req, res) => {
    let event = new eventModel({
			id : req.body.id,
			description : req.body.description
    });

    event.save((err, event) => {
      if (err) {
        return res.status(500).json({
          message: 'Error when creating event',
          error: err
        });
      }
      return res.status(201).json(event);
    });
  },

  /**
   * eventController.update()
   */
  update: (req, res) => {
    let id = req.params.id;
    eventModel.findOne({_id: id}, (err, event) => {
      if (err) {
        return res.status(500).json({
          message: 'Error when getting event',
          error: err
        });
      }
      if (!event) {
        return res.status(404).json({
          message: 'No such event'
        });
      }

      event.id = req.body.id ? req.body.id : event.id;
			event.description = req.body.description ? req.body.description : event.description;
			
      event.save( (err, event) => {
        if (err) {
          return res.status(500).json({
            message: 'Error when updating event.',
            error: err
          });
        }

        return res.json(event);
      });
    });
  },

  /**
   * eventController.remove()
   */
  remove: (req, res) => {
    let id = req.params.id;
    eventModel.findByIdAndRemove(id, (err, event) => {
      if (err) {
        return res.status(500).json({
          message: 'Error when deleting the event.',
          error: err
        });
      }
      return res.status(204).json();
    });
  }
};
